library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of col_det_parent is


signal value1, value2, result :std_logic_vector(7 downto 0) := "00000000";
signal done, start, started :std_logic;
signal x_circle, x_block_0, x_block_1 :std_logic_vector(6 downto 0) := "0000000";
signal y_circle, y_block :std_logic_vector(7 downto 0) := "00000000";


component hitman2 is
	port(clk    :in    std_logic;
        reset  :in    std_logic;
        x_red  :in    std_logic_vector(8 downto 0);
        x_blue :in    std_logic_vector(8 downto 0);
        y_red  :in    std_logic_vector(7 downto 0);
        y_blue :in    std_logic_vector(7 downto 0);
        x00    :in    std_logic_vector(8 downto 0);
        x01    :in    std_logic_vector(8 downto 0);
        x10    :in    std_logic_vector(8 downto 0);
        x11    :in    std_logic_vector(8 downto 0);
        height :in    std_logic_vector(5 downto 0);
        y0     :in    std_logic_vector(7 downto 0);
        y1     :in    std_logic_vector(7 downto 0);
				done   :in    std_logic;
				started:in    std_logic;
				start  :out   std_logic;
				x_circle, x_block_0, x_block_1 :out std_logic_vector(6 downto 0) := "0000000";
				y_circle, y_block              :out std_logic_vector(7 downto 0) := "00000000"
);
end component;

component hit_math_unit is
   port(clk      :in	std_logic;
		reset    :in	std_logic;
		start    :in	std_logic;
		x_circle :in	std_logic_vector(6 downto 0);
		y_circle :in	std_logic_vector(7 downto 0);
		x_block_0:in	std_logic_vector(6 downto 0);
		x_block_1:in	std_logic_vector(6 downto 0);
		y_block  :in	std_logic_vector(7 downto 0);
		height   :in	std_logic_vector(5 downto 0);
		value1   :out	std_logic_vector(7 downto 0);
		value2   :out	std_logic_vector(7 downto 0);
		result   :in	std_logic_vector(7 downto 0);
		started  :out	std_logic;
		done     :out	std_logic;
		rip      :out	std_logic);
end component;

component subtraction is
	port(clk   :in    std_logic;
		reset  :in    std_logic;
		value1 :in    std_logic_vector(7 downto 0);
		value2 :in    std_logic_vector(7 downto 0);
		result :out   std_logic_vector(7 downto 0));
end component;

begin

hit_states: hitman2 port map(
		clk => clk,
		reset => reset,
		x_red => x_red,
		x_blue => x_blue,
		y_red => y_red,
		y_blue => y_blue,
		x00 => x00,
		x01 => x01,
		x10 => x10,
		x11 => x11,
		height => height,
		y0 => y0,
		y1 => y1,
		done => done,
		started => started,
		start => start,
		x_circle => x_circle,
		x_block_0 => x_block_0,
		x_block_1 => x_block_1,
		y_circle => y_circle,
		y_block => y_block
);

math_unit: hit_math_unit port map (
		clk => clk,
		reset => reset,
		start => start,
		x_circle => x_circle,
		y_circle => y_circle,
		x_block_0 => x_block_0,
		x_block_1 => x_block_1,
		y_block => y_block,
		height => height,
		value1 => value1,
		value2 => value2,
		result => result,
		started => started,
		done => done,
		rip => rip
);

-- Result = value1 - value2;
subtractor: subtraction port map (
	clk => clk,
	reset => reset,
	value1 => value1,
	value2 => value2,
	result => result
);

end behaviour;