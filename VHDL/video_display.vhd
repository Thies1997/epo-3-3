library IEEE;
use IEEE.std_logic_1164.ALL;

entity video_display is
   port(clk		   :in	  std_logic;
	row        :in    std_logic_vector(8 downto 0);
        column     :in    std_logic_vector(7 downto 0);
        row_address:out   std_logic_vector(6 downto 0);
        col_address:out   std_logic_vector(7 downto 0));
end video_display;

