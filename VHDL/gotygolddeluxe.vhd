library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.all;

entity gotygolddeluxe is
	port(clk         : in     std_logic;
		reset         : in     std_logic;
		left          : in     std_logic;
		right         : in     std_logic;
		red,green,blue: out    std_logic;
		hsync         : out    std_logic;
		vsync         : out    std_logic;
		rip           : inout  std_logic;
		cnt           : inout  std_logic;
		enable_reset  : in     std_logic
	);
end gotygolddeluxe;




















