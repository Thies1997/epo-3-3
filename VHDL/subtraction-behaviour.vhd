library IEEE;
use IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

architecture behaviour of subtraction is
begin
P1: process(clk, reset)
begin

if (clk'event and clk = '0') then
	if (reset = '1') then
		result <= "00000000";
	else
		result <= std_logic_vector(unsigned(value1(7 downto 0)) - unsigned(value2(7 downto 0)));
	end if;
end if;

end process;
end behaviour;





