
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of hitman2 is

   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component mu111
      port( A, B, S : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component dfr11
      port( D, R, CK : in std_logic;  Q : out std_logic);
   end component;
   
   component dfn10
      port( D, CK : in std_logic;  Q : out std_logic);
   end component;
   
   signal current_state_2_port, current_state_1_port, current_state_0_port, 
      next_state_2_port, next_state_1_port, next_state_0_port, N41, N42, n1, n2
      , n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, 
      n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93
      , n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106,
      n107, n108, n109, n110, n111, n112, n113, n114, n115, n116, n117, n118, 
      n119, n120, n121, n122, n123, n124, n125, n126 : std_logic;

begin
   
   start_reg : dfr11 port map( D => n1, R => started, CK => clk, Q => start);
   next_state_reg_0_inst : dfn10 port map( D => N41, CK => done, Q => 
                           next_state_0_port);
   current_state_reg_0_inst : dfr11 port map( D => next_state_0_port, R => 
                           reset, CK => clk, Q => current_state_0_port);
   next_state_reg_2_inst : dfn10 port map( D => n2, CK => done, Q => 
                           next_state_2_port);
   current_state_reg_2_inst : dfr11 port map( D => next_state_2_port, R => 
                           reset, CK => clk, Q => current_state_2_port);
   next_state_reg_1_inst : dfn10 port map( D => N42, CK => done, Q => 
                           next_state_1_port);
   current_state_reg_1_inst : dfr11 port map( D => next_state_1_port, R => 
                           reset, CK => clk, Q => current_state_1_port);
   U95 : na210 port map( A => n115, B => n116, Y => n65);
   U96 : no210 port map( A => n125, B => current_state_2_port, Y => n66);
   U97 : na210 port map( A => n67, B => n68, Y => y_circle(7));
   U98 : na210 port map( A => y_blue(7), B => n69, Y => n68);
   U99 : na210 port map( A => y_red(7), B => n70, Y => n67);
   U100 : na210 port map( A => n71, B => n72, Y => y_circle(6));
   U101 : na210 port map( A => y_blue(6), B => n65, Y => n72);
   U102 : na210 port map( A => y_red(6), B => n66, Y => n71);
   U103 : na210 port map( A => n73, B => n74, Y => y_circle(5));
   U104 : na210 port map( A => y_blue(5), B => n69, Y => n74);
   U105 : na210 port map( A => y_red(5), B => n70, Y => n73);
   U106 : na210 port map( A => n75, B => n76, Y => y_circle(4));
   U107 : na210 port map( A => y_blue(4), B => n65, Y => n76);
   U108 : na210 port map( A => y_red(4), B => n66, Y => n75);
   U109 : na210 port map( A => n77, B => n78, Y => y_circle(3));
   U110 : na210 port map( A => y_blue(3), B => n69, Y => n78);
   U111 : na210 port map( A => y_red(3), B => n70, Y => n77);
   U112 : na210 port map( A => n79, B => n80, Y => y_circle(2));
   U113 : na210 port map( A => y_blue(2), B => n65, Y => n80);
   U114 : na210 port map( A => y_red(2), B => n66, Y => n79);
   U115 : na210 port map( A => n81, B => n82, Y => y_circle(1));
   U116 : na210 port map( A => y_blue(1), B => n69, Y => n82);
   U117 : na210 port map( A => y_red(1), B => n70, Y => n81);
   U118 : na210 port map( A => n83, B => n84, Y => y_circle(0));
   U119 : na210 port map( A => y_blue(0), B => n65, Y => n84);
   U120 : na210 port map( A => y_red(0), B => n66, Y => n83);
   U121 : na210 port map( A => n85, B => n86, Y => y_block(7));
   U122 : na210 port map( A => y1(7), B => n69, Y => n86);
   U123 : na210 port map( A => y0(7), B => n70, Y => n85);
   U124 : na210 port map( A => n87, B => n88, Y => y_block(6));
   U125 : na210 port map( A => y1(6), B => n65, Y => n88);
   U126 : na210 port map( A => y0(6), B => n66, Y => n87);
   U127 : na210 port map( A => n89, B => n90, Y => y_block(5));
   U128 : na210 port map( A => y1(5), B => n69, Y => n90);
   U129 : na210 port map( A => y0(5), B => n70, Y => n89);
   U130 : na210 port map( A => n91, B => n92, Y => y_block(4));
   U131 : na210 port map( A => y1(4), B => n65, Y => n92);
   U132 : na210 port map( A => y0(4), B => n66, Y => n91);
   U133 : na210 port map( A => n93, B => n94, Y => y_block(3));
   U134 : na210 port map( A => y1(3), B => n69, Y => n94);
   U135 : na210 port map( A => y0(3), B => n70, Y => n93);
   U136 : na210 port map( A => n95, B => n96, Y => y_block(2));
   U137 : na210 port map( A => y1(2), B => n65, Y => n96);
   U138 : na210 port map( A => y0(2), B => n66, Y => n95);
   U139 : na210 port map( A => n97, B => n98, Y => y_block(1));
   U140 : na210 port map( A => y1(1), B => n69, Y => n98);
   U141 : na210 port map( A => y0(1), B => n70, Y => n97);
   U142 : na210 port map( A => n99, B => n100, Y => y_block(0));
   U143 : na210 port map( A => y1(0), B => n65, Y => n100);
   U144 : na210 port map( A => y0(0), B => n66, Y => n99);
   U145 : na210 port map( A => n101, B => n102, Y => x_circle(6));
   U146 : na210 port map( A => x_blue(6), B => n69, Y => n102);
   U147 : na210 port map( A => x_red(6), B => n70, Y => n101);
   U148 : na210 port map( A => n103, B => n104, Y => x_circle(5));
   U149 : na210 port map( A => x_blue(5), B => n65, Y => n104);
   U150 : na210 port map( A => x_red(5), B => n66, Y => n103);
   U151 : na210 port map( A => n105, B => n106, Y => x_circle(4));
   U152 : na210 port map( A => x_blue(4), B => n69, Y => n106);
   U153 : na210 port map( A => x_red(4), B => n70, Y => n105);
   U154 : na210 port map( A => n107, B => n108, Y => x_circle(3));
   U155 : na210 port map( A => x_blue(3), B => n65, Y => n108);
   U156 : na210 port map( A => x_red(3), B => n66, Y => n107);
   U157 : na210 port map( A => n109, B => n110, Y => x_circle(2));
   U158 : na210 port map( A => x_blue(2), B => n69, Y => n110);
   U159 : na210 port map( A => x_red(2), B => n70, Y => n109);
   U160 : na210 port map( A => n111, B => n112, Y => x_circle(1));
   U161 : na210 port map( A => x_blue(1), B => n65, Y => n112);
   U162 : na210 port map( A => x_red(1), B => n66, Y => n111);
   U163 : na210 port map( A => n113, B => n114, Y => x_circle(0));
   U164 : na210 port map( A => x_blue(0), B => n69, Y => n114);
   U165 : na210 port map( A => n115, B => n116, Y => n69);
   U166 : na210 port map( A => current_state_1_port, B => N41, Y => n116);
   U167 : na210 port map( A => x_red(0), B => n70, Y => n113);
   U168 : iv110 port map( A => n117, Y => n2);
   U169 : iv110 port map( A => reset, Y => n1);
   U170 : na210 port map( A => n118, B => n119, Y => block_type(2));
   U171 : na210 port map( A => type1(2), B => n120, Y => n119);
   U172 : na210 port map( A => type0(2), B => N42, Y => n118);
   U173 : na210 port map( A => n121, B => n122, Y => block_type(1));
   U174 : na210 port map( A => type1(1), B => n120, Y => n122);
   U175 : na210 port map( A => type0(1), B => N42, Y => n121);
   U176 : na210 port map( A => n123, B => n124, Y => block_type(0));
   U177 : na210 port map( A => type1(0), B => n120, Y => n124);
   U178 : na210 port map( A => n117, B => n115, Y => n120);
   U179 : na310 port map( A => n125, B => n126, C => current_state_2_port, Y =>
                           n115);
   U180 : iv110 port map( A => current_state_1_port, Y => n126);
   U181 : na210 port map( A => n66, B => current_state_1_port, Y => n117);
   U182 : na210 port map( A => type0(0), B => N42, Y => n123);
   U183 : mu111 port map( A => n70, B => N41, S => current_state_1_port, Y => 
                           N42);
   U184 : no210 port map( A => n125, B => current_state_2_port, Y => n70);
   U185 : iv110 port map( A => current_state_0_port, Y => n125);
   U186 : no210 port map( A => current_state_0_port, B => current_state_2_port,
                           Y => N41);

end synthesised;



