library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of dec_score is

signal tentussen,numtussen : std_logic_vector(3 downto 0);

component numbers is
	port ( clk	: in std_logic;
	       reset : in std_logic;
	       score : out std_logic_vector(3 downto 0));
end component;

component big_number is
   port(clk  :in    std_logic;
        reset:in    std_logic;
        a:in    std_logic_vector(3 downto 0);
        b  :out   std_logic_vector(3 downto 0));
end component;

begin
lbl1: numbers port map (clk, reset, numtussen);
lbl2: big_number port map (clk, reset, numtussen, tentussen);
lbl3: big_number port map (clk, reset, tentussen, hun);
num <= numtussen;
ten <= tentussen;
end behaviour;