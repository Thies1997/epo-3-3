library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of bar is

signal base_y, bar_y0, bar_y1, bar_y2, bar_y3: std_logic_vector(7 downto 0);
signal bar_type0, bar_type1, bar_type2, bar_type3: std_logic_vector(2 downto 0);
-- signal do_reset, did_reset : std_logic;

begin

output_y0 <= bar_y0;
output_y1 <= bar_y1;
output_y2 <= bar_y2;
output_y3 <= bar_y3;

output_type0 <= bar_type0;
output_type1 <= bar_type1;
output_type2 <= bar_type2;
output_type3 <= bar_type3;

-- Maybe bar_y1, bar_y2, bar_y3, bar_type0, bar_type1, bar_type2, bar_type3
-- P1: process (clk, reset, counter, did_reset)
-- begin
	-- if (rising_edge(clk)) then
		-- if (reset = '1') then   
			-- do_reset <= '1';
		-- else
			-- if (did_reset = '1' and counter = '1') then
				-- do_reset <= '0';
			-- else
				-- do_reset <= '1';
			-- end if;
		-- end if;
	-- end if;
-- end process;

P1: process (counter, reset, enable_reset)
begin
	if (rising_edge(counter)) then
		if (reset = '1' or enable_reset = '1') then
			bar_y0 <= "00000000";
			bar_y1 <= "00000000";
			bar_y2 <= "00000000";
			bar_y3 <= "00000000";
			
			bar_type0 <= "000";
			bar_type1 <= "000";
			bar_type2 <= "000";
			bar_type3 <= "000";
		else
			if(base_y(7)='1' AND base_y(5)='1') then
				bar_y0 <= "01111000";
				bar_y1 <= "01010000";
				bar_y2 <= "00101000";
				bar_y3 <= "00000000";
				
				bar_type0 <= bar_type1;
				bar_type1 <= bar_type2;
				bar_type2 <= bar_type3;
				bar_type3 <= blocktype;
			elsif(base_y > "01111000") then
				bar_y0 <= base_y;
				bar_y1 <= std_logic_vector(unsigned(base_y) - 40);
				bar_y2 <= std_logic_vector(unsigned(base_y) - 80);
				bar_y3 <= std_logic_vector(unsigned(base_y) - 120);
				
				bar_type0 <= bar_type0;
				bar_type1 <= bar_type1;
				bar_type2 <= bar_type2;
				bar_type3 <= bar_type3;
			elsif(base_y(6)='1' AND base_y(5)='1' AND base_y(4)='1' AND base_y(3)='1') then
				bar_y0 <= base_y;
				bar_y1 <= std_logic_vector(unsigned(base_y) - 40);
				bar_y2 <= std_logic_vector(unsigned(base_y) - 80);
				bar_y3 <= std_logic_vector(unsigned(base_y) - 120);
				
				bar_type0 <= "101";
				bar_type1 <= "010";
				bar_type2 <= "001";
				bar_type3 <= "110";
			elsif(base_y > "01010000") then
				bar_y0 <= base_y;
				bar_y1 <= std_logic_vector(unsigned(base_y) - 40);
				bar_y2 <= std_logic_vector(unsigned(base_y) - 80); 
				bar_y3 <= "00000000";
				
				bar_type0 <= "101";
				bar_type1 <= "010";
				bar_type2 <= "001";
				bar_type3 <= "000";
			elsif(base_y > "00101000") then
				bar_y0 <= base_y;
				bar_y1 <= std_logic_vector(unsigned(base_y) - 40);
				bar_y2 <= "00000000";
				bar_y3 <= "00000000";
				
				bar_type0 <= "101";
				bar_type1 <= "010";
				bar_type2 <= "000";
				bar_type3 <= "000";
			elsif(base_y > "00000001") then 
				bar_y0 <= base_y;
				bar_y1 <= "00000000";
				bar_y2 <= "00000000";
				bar_y3 <= "00000000";
				
				bar_type0 <= "101";
				bar_type1 <= "000";
				bar_type2 <= "000";
				bar_type3 <= "000";
			else 
				bar_y0 <= base_y;
				bar_y1 <= "00000000";
				bar_y2 <= "00000000";
				bar_y3 <= "00000000";

				bar_type0 <= "101";
				bar_type1 <= "000";
				bar_type2 <= "000";
				bar_type3 <= "000";
			end if;
		end if;
	end if;
end process;

P2: process (bar_y0)
begin
	if (enable = '0') then
        base_y <= std_logic_vector(unsigned(bar_y0) + 1);
    end if;
end process;

end behaviour;
