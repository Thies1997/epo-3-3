library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;

architecture behaviour of numbers is
signal clock_count, new_clock_count : unsigned (5 downto 0);
signal score_count, new_score_count : unsigned (3 downto 0);

begin
process (clk)
	begin
		if (clk'event and clk = '1') then
			if (reset = '1') then
				clock_count <= (others => '0');
				score_count <= (others => '0');
			else 
				clock_count <= new_clock_count;
				score_count <= new_score_count;
			end if;
		end if;
	end process;

process (clock_count, score_count) -- newscore = score+1
	begin

		if(clock_count = "111100") then -- 60 Hz clk
			new_clock_count <= "000000";
			new_score_count <= score_count + 1;
		elsif(score_count = "1010") then
			new_clock_count <= "000000";
			new_score_count <= "0000"; 
		else
			new_clock_count <= clock_count + 1;
			new_score_count <= score_count;
			
		end if;	
	end process;

score <= std_logic_vector(score_count);

end behaviour;
