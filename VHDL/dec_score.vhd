library IEEE;
use IEEE.std_logic_1164.ALL;

entity dec_score is
   port(clk  :in    std_logic;
        reset:in    std_logic;
        hun  :out   std_logic_vector(3 downto 0);
        ten  :out   std_logic_vector(3 downto 0);
        num  :out   std_logic_vector(3 downto 0));
end dec_score;
