configuration vga_module_structural_cfg of vga_module is
   for structural
      for all: vga_sync use configuration work.vga_sync_behaviour_cfg;
      end for;
      for all: vga_hub use configuration work.vga_hub_structural_cfg;
      end for;
   end for;
end vga_module_structural_cfg;


