
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of hit_math_unit is

   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component ex210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component no310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component mu111
      port( A, B, S : in std_logic;  Y : out std_logic);
   end component;
   
   component na310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component dfn10
      port( D, CK : in std_logic;  Q : out std_logic);
   end component;
   
   component dfr11
      port( D, R, CK : in std_logic;  Q : out std_logic);
   end component;
   
   signal done_port, current_state_2_port, current_state_1_port, 
      current_state_0_port, current_checks_3_port, current_checks_2_port, 
      current_checks_1_port, current_checks_0_port, N12, N13, N14, N15, N16, 
      N17, N18, N19, y_bottom_7_port, y_bottom_6_port, y_bottom_5_port, 
      y_bottom_4_port, y_bottom_3_port, y_bottom_2_port, y_bottom_1_port, 
      y_bottom_0_port, n1, n115, n116, n117, n118, n119, n120, n121, n122, n127
      , n128, n129, n130, n131, n132, n133, n134, n135, n136, n137, n138, n139,
      n140, n141, n142, n143, n144, n145, n146, n147, n148, n149, n150, n151, 
      n152, n153, n154, n155, n156, n157, n158, n159, n160, n161, n162, n163, 
      n164, n165, n166, n167, n168, n169, n170, n171, n172, n173, n174, n175, 
      n176, n177, n178, n179, n180, n181, n182, n183, n184, n185, n186, n187, 
      n188, n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199, 
      n200, n201, n202, n203, n204, n205, n206, n207, n208, n209, n210, n211, 
      n212, n213, n214, n215, n216, n217, n218, n219, n220, n222, n223, 
      add_23_n37, add_23_n36, add_23_n35, add_23_n34, add_23_n33, add_23_n32, 
      add_23_n31, add_23_n30, add_23_n29, add_23_n28, add_23_n27, add_23_n26, 
      add_23_n25, add_23_n24, add_23_n23, add_23_n22, add_23_n21, add_23_n20, 
      add_23_n19, add_23_n18, add_23_n17, add_23_n16, add_23_n15, add_23_n14, 
      add_23_n13, add_23_n12, add_23_n11, add_23_n10, add_23_n9, add_23_n8, 
      add_23_n7, add_23_n6, add_23_n5, add_23_n4, add_23_n3, add_23_n2 : 
      std_logic;

begin
   done <= done_port;
   
   n1 <= '0';
   y_bottom_reg_7_inst : dfn10 port map( D => n122, CK => clk, Q => 
                           y_bottom_7_port);
   y_bottom_reg_6_inst : dfn10 port map( D => n121, CK => clk, Q => 
                           y_bottom_6_port);
   y_bottom_reg_5_inst : dfn10 port map( D => n120, CK => clk, Q => 
                           y_bottom_5_port);
   y_bottom_reg_4_inst : dfn10 port map( D => n119, CK => clk, Q => 
                           y_bottom_4_port);
   y_bottom_reg_3_inst : dfn10 port map( D => n118, CK => clk, Q => 
                           y_bottom_3_port);
   y_bottom_reg_2_inst : dfn10 port map( D => n117, CK => clk, Q => 
                           y_bottom_2_port);
   y_bottom_reg_1_inst : dfn10 port map( D => n116, CK => clk, Q => 
                           y_bottom_1_port);
   y_bottom_reg_0_inst : dfn10 port map( D => n115, CK => clk, Q => 
                           y_bottom_0_port);
   current_state_reg_0_inst : dfr11 port map( D => n131, R => reset, CK => clk,
                           Q => current_state_0_port);
   current_state_reg_2_inst : dfr11 port map( D => n222, R => reset, CK => clk,
                           Q => current_state_2_port);
   current_state_reg_1_inst : dfr11 port map( D => n223, R => reset, CK => clk,
                           Q => current_state_1_port);
   current_checks_reg_3_inst : dfn10 port map( D => n130, CK => clk, Q => 
                           current_checks_3_port);
   current_checks_reg_2_inst : dfn10 port map( D => n129, CK => clk, Q => 
                           current_checks_2_port);
   current_checks_reg_1_inst : dfn10 port map( D => n128, CK => clk, Q => 
                           current_checks_1_port);
   current_checks_reg_0_inst : dfn10 port map( D => n127, CK => clk, Q => 
                           current_checks_0_port);
   U150 : iv110 port map( A => n203, Y => n132);
   U151 : na210 port map( A => n133, B => n134, Y => value2(7));
   U152 : na210 port map( A => y_bottom_7_port, B => n135, Y => n134);
   U153 : na210 port map( A => y_block(7), B => n136, Y => n133);
   U154 : na310 port map( A => n137, B => n138, C => n139, Y => value2(6));
   U155 : iv110 port map( A => n140, Y => n139);
   U156 : na210 port map( A => n141, B => n142, Y => n140);
   U157 : na210 port map( A => n136, B => y_block(6), Y => n142);
   U158 : na210 port map( A => n135, B => y_bottom_6_port, Y => n141);
   U159 : na210 port map( A => x_block_0(6), B => n143, Y => n138);
   U160 : na210 port map( A => x_block_1(6), B => n144, Y => n137);
   U161 : na310 port map( A => n145, B => n146, C => n147, Y => value2(5));
   U162 : iv110 port map( A => n148, Y => n147);
   U163 : na210 port map( A => n149, B => n150, Y => n148);
   U164 : na210 port map( A => n136, B => y_block(5), Y => n150);
   U165 : na210 port map( A => n135, B => y_bottom_5_port, Y => n149);
   U166 : na210 port map( A => x_block_0(5), B => n143, Y => n146);
   U167 : na210 port map( A => x_block_1(5), B => n144, Y => n145);
   U168 : na310 port map( A => n151, B => n152, C => n153, Y => value2(4));
   U169 : iv110 port map( A => n154, Y => n153);
   U170 : na210 port map( A => n155, B => n156, Y => n154);
   U171 : na210 port map( A => n136, B => y_block(4), Y => n156);
   U172 : na210 port map( A => n135, B => y_bottom_4_port, Y => n155);
   U173 : na210 port map( A => x_block_0(4), B => n143, Y => n152);
   U174 : na210 port map( A => x_block_1(4), B => n144, Y => n151);
   U175 : na310 port map( A => n157, B => n158, C => n159, Y => value2(3));
   U176 : iv110 port map( A => n160, Y => n159);
   U177 : na210 port map( A => n161, B => n162, Y => n160);
   U178 : na210 port map( A => n136, B => y_block(3), Y => n162);
   U179 : na210 port map( A => n135, B => y_bottom_3_port, Y => n161);
   U180 : na210 port map( A => x_block_0(3), B => n143, Y => n158);
   U181 : na210 port map( A => x_block_1(3), B => n144, Y => n157);
   U182 : na310 port map( A => n163, B => n164, C => n165, Y => value2(2));
   U183 : iv110 port map( A => n166, Y => n165);
   U184 : na210 port map( A => n167, B => n168, Y => n166);
   U185 : na210 port map( A => n136, B => y_block(2), Y => n168);
   U186 : na210 port map( A => n135, B => y_bottom_2_port, Y => n167);
   U187 : na210 port map( A => x_block_0(2), B => n143, Y => n164);
   U188 : na210 port map( A => x_block_1(2), B => n144, Y => n163);
   U189 : na310 port map( A => n169, B => n170, C => n171, Y => value2(1));
   U190 : iv110 port map( A => n172, Y => n171);
   U191 : na210 port map( A => n173, B => n174, Y => n172);
   U192 : na210 port map( A => n136, B => y_block(1), Y => n174);
   U193 : na210 port map( A => n135, B => y_bottom_1_port, Y => n173);
   U194 : na210 port map( A => x_block_0(1), B => n143, Y => n170);
   U195 : na210 port map( A => x_block_1(1), B => n144, Y => n169);
   U196 : na310 port map( A => n175, B => n176, C => n177, Y => value2(0));
   U197 : iv110 port map( A => n178, Y => n177);
   U198 : na210 port map( A => n179, B => n180, Y => n178);
   U199 : na210 port map( A => n136, B => y_block(0), Y => n180);
   U200 : na210 port map( A => n135, B => y_bottom_0_port, Y => n179);
   U201 : na210 port map( A => x_block_0(0), B => n143, Y => n176);
   U202 : na210 port map( A => x_block_1(0), B => n144, Y => n175);
   U203 : na210 port map( A => n181, B => n182, Y => value1(6));
   U204 : na210 port map( A => x_circle(6), B => n223, Y => n182);
   U205 : na210 port map( A => y_circle(6), B => n222, Y => n181);
   U206 : na210 port map( A => n183, B => n184, Y => value1(5));
   U207 : na210 port map( A => x_circle(5), B => n223, Y => n184);
   U208 : na210 port map( A => y_circle(5), B => n222, Y => n183);
   U209 : na210 port map( A => n185, B => n186, Y => value1(4));
   U210 : na210 port map( A => x_circle(4), B => n223, Y => n186);
   U211 : na210 port map( A => y_circle(4), B => n222, Y => n185);
   U212 : na210 port map( A => n187, B => n188, Y => value1(3));
   U213 : na210 port map( A => x_circle(3), B => n223, Y => n188);
   U214 : na210 port map( A => y_circle(3), B => n222, Y => n187);
   U215 : na210 port map( A => n189, B => n190, Y => value1(2));
   U216 : na210 port map( A => x_circle(2), B => n223, Y => n190);
   U217 : na210 port map( A => y_circle(2), B => n222, Y => n189);
   U218 : na210 port map( A => n191, B => n192, Y => value1(1));
   U219 : na210 port map( A => x_circle(1), B => n223, Y => n192);
   U220 : na210 port map( A => y_circle(1), B => n222, Y => n191);
   U221 : na210 port map( A => n193, B => n194, Y => value1(0));
   U222 : na210 port map( A => x_circle(0), B => n223, Y => n194);
   U223 : na210 port map( A => y_circle(0), B => n222, Y => n193);
   U224 : na310 port map( A => n195, B => n196, C => n197, Y => started);
   U225 : iv110 port map( A => n198, Y => value1(7));
   U226 : na210 port map( A => y_circle(7), B => n222, Y => n198);
   U227 : iv110 port map( A => n196, Y => n222);
   U228 : iv110 port map( A => n195, Y => n223);
   U229 : no310 port map( A => n199, B => n200, C => n201, Y => rip);
   U230 : iv110 port map( A => current_checks_3_port, Y => n201);
   U231 : iv110 port map( A => current_checks_2_port, Y => n200);
   U232 : na310 port map( A => current_checks_0_port, B => done_port, C => 
                           current_checks_1_port, Y => n199);
   U233 : na310 port map( A => n202, B => n203, C => n197, Y => n131);
   U234 : na210 port map( A => start, B => n204, Y => n197);
   U235 : no210 port map( A => current_state_0_port, B => current_state_1_port,
                           Y => n204);
   U236 : mu111 port map( A => current_checks_3_port, B => n205, S => n206, Y 
                           => n130);
   U237 : no210 port map( A => n136, B => n207, Y => n206);
   U238 : no210 port map( A => n203, B => n208, Y => n205);
   U239 : iv110 port map( A => n135, Y => n203);
   U240 : mu111 port map( A => current_checks_2_port, B => n209, S => n210, Y 
                           => n129);
   U241 : no210 port map( A => n135, B => n207, Y => n210);
   U242 : na210 port map( A => n195, B => n211, Y => n207);
   U243 : no210 port map( A => n143, B => n144, Y => n195);
   U244 : no210 port map( A => result(7), B => n212, Y => n209);
   U245 : mu111 port map( A => current_checks_1_port, B => n213, S => n214, Y 
                           => n128);
   U246 : no210 port map( A => n143, B => n215, Y => n214);
   U247 : no210 port map( A => n202, B => n208, Y => n213);
   U248 : mu111 port map( A => current_checks_0_port, B => n216, S => n217, Y 
                           => n127);
   U249 : no210 port map( A => n144, B => n215, Y => n217);
   U250 : na210 port map( A => n196, B => n211, Y => n215);
   U251 : iv110 port map( A => reset, Y => n211);
   U252 : no210 port map( A => n132, B => n136, Y => n196);
   U253 : iv110 port map( A => n212, Y => n136);
   U254 : na310 port map( A => current_state_0_port, B => n218, C => 
                           current_state_1_port, Y => n212);
   U255 : no310 port map( A => current_state_0_port, B => current_state_1_port,
                           C => n218, Y => n135);
   U256 : iv110 port map( A => n202, Y => n144);
   U257 : na310 port map( A => n219, B => n218, C => current_state_1_port, Y =>
                           n202);
   U258 : iv110 port map( A => n220, Y => n216);
   U259 : na210 port map( A => n208, B => n143, Y => n220);
   U260 : no310 port map( A => current_state_1_port, B => current_state_2_port,
                           C => n219, Y => n143);
   U261 : iv110 port map( A => result(7), Y => n208);
   U262 : mu111 port map( A => N19, B => y_bottom_7_port, S => reset, Y => n122
                           );
   U263 : mu111 port map( A => N18, B => y_bottom_6_port, S => reset, Y => n121
                           );
   U264 : mu111 port map( A => N17, B => y_bottom_5_port, S => reset, Y => n120
                           );
   U265 : mu111 port map( A => N16, B => y_bottom_4_port, S => reset, Y => n119
                           );
   U266 : mu111 port map( A => N15, B => y_bottom_3_port, S => reset, Y => n118
                           );
   U267 : mu111 port map( A => N14, B => y_bottom_2_port, S => reset, Y => n117
                           );
   U268 : mu111 port map( A => N13, B => y_bottom_1_port, S => reset, Y => n116
                           );
   U269 : mu111 port map( A => N12, B => y_bottom_0_port, S => reset, Y => n115
                           );
   U270 : no310 port map( A => n218, B => current_state_1_port, C => n219, Y =>
                           done_port);
   U271 : iv110 port map( A => current_state_0_port, Y => n219);
   U272 : iv110 port map( A => current_state_2_port, Y => n218);
   add_23_U44 : ex210 port map( A => height(0), B => y_block(0), Y => N12);
   add_23_U43 : na210 port map( A => height(0), B => y_block(0), Y => 
                           add_23_n37);
   add_23_U42 : ex210 port map( A => height(1), B => y_block(1), Y => 
                           add_23_n36);
   add_23_U41 : ex210 port map( A => add_23_n9, B => add_23_n36, Y => N13);
   add_23_U40 : na210 port map( A => y_block(1), B => add_23_n9, Y => 
                           add_23_n33);
   add_23_U39 : no210 port map( A => y_block(1), B => add_23_n9, Y => 
                           add_23_n35);
   add_23_U38 : na210 port map( A => height(1), B => add_23_n8, Y => add_23_n34
                           );
   add_23_U37 : na210 port map( A => add_23_n33, B => add_23_n34, Y => 
                           add_23_n31);
   add_23_U36 : ex210 port map( A => height(2), B => y_block(2), Y => 
                           add_23_n32);
   add_23_U35 : ex210 port map( A => add_23_n31, B => add_23_n32, Y => N14);
   add_23_U34 : ex210 port map( A => add_23_n5, B => height(3), Y => add_23_n27
                           );
   add_23_U33 : na210 port map( A => y_block(2), B => add_23_n31, Y => 
                           add_23_n28);
   add_23_U32 : no210 port map( A => add_23_n31, B => y_block(2), Y => 
                           add_23_n30);
   add_23_U31 : na210 port map( A => height(2), B => add_23_n7, Y => add_23_n29
                           );
   add_23_U30 : na210 port map( A => add_23_n28, B => add_23_n29, Y => 
                           add_23_n26);
   add_23_U29 : ex210 port map( A => add_23_n27, B => add_23_n6, Y => N15);
   add_23_U28 : na210 port map( A => y_block(3), B => add_23_n26, Y => 
                           add_23_n23);
   add_23_U27 : na210 port map( A => add_23_n6, B => add_23_n5, Y => add_23_n25
                           );
   add_23_U26 : na210 port map( A => height(3), B => add_23_n25, Y => 
                           add_23_n24);
   add_23_U25 : na210 port map( A => add_23_n23, B => add_23_n24, Y => 
                           add_23_n21);
   add_23_U24 : ex210 port map( A => height(4), B => y_block(4), Y => 
                           add_23_n22);
   add_23_U23 : ex210 port map( A => add_23_n21, B => add_23_n22, Y => N16);
   add_23_U22 : ex210 port map( A => add_23_n2, B => height(5), Y => add_23_n17
                           );
   add_23_U21 : na210 port map( A => y_block(4), B => add_23_n21, Y => 
                           add_23_n18);
   add_23_U20 : no210 port map( A => add_23_n21, B => y_block(4), Y => 
                           add_23_n20);
   add_23_U19 : na210 port map( A => height(4), B => add_23_n4, Y => add_23_n19
                           );
   add_23_U18 : na210 port map( A => add_23_n18, B => add_23_n19, Y => 
                           add_23_n16);
   add_23_U17 : ex210 port map( A => add_23_n17, B => add_23_n3, Y => N17);
   add_23_U16 : na210 port map( A => y_block(5), B => add_23_n16, Y => 
                           add_23_n13);
   add_23_U15 : na210 port map( A => add_23_n3, B => add_23_n2, Y => add_23_n15
                           );
   add_23_U14 : na210 port map( A => height(5), B => add_23_n15, Y => 
                           add_23_n14);
   add_23_U13 : na210 port map( A => add_23_n13, B => add_23_n14, Y => 
                           add_23_n12);
   add_23_U12 : ex210 port map( A => add_23_n12, B => y_block(6), Y => N18);
   add_23_U11 : na210 port map( A => add_23_n12, B => y_block(6), Y => 
                           add_23_n11);
   add_23_U10 : ex210 port map( A => y_block(7), B => add_23_n11, Y => 
                           add_23_n10);
   add_23_U9 : iv110 port map( A => add_23_n37, Y => add_23_n9);
   add_23_U8 : iv110 port map( A => add_23_n35, Y => add_23_n8);
   add_23_U7 : iv110 port map( A => add_23_n30, Y => add_23_n7);
   add_23_U6 : iv110 port map( A => add_23_n26, Y => add_23_n6);
   add_23_U5 : iv110 port map( A => y_block(3), Y => add_23_n5);
   add_23_U4 : iv110 port map( A => add_23_n20, Y => add_23_n4);
   add_23_U3 : iv110 port map( A => add_23_n16, Y => add_23_n3);
   add_23_U2 : iv110 port map( A => y_block(5), Y => add_23_n2);
   add_23_U1 : iv110 port map( A => add_23_n10, Y => N19);

end synthesised;



